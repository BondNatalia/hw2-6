// 1 Це коли потрібно використовувати спеціальний символ в якості звичайного символу, для цього до спец символу додається , наприклад, \
//2 function f() {}; const f = function();
//3 Це переміщення декларування змінної вгору блоку за замовчуванням


function createNewUser () {
    const newUser = {
        firstName: prompt("Введіть ім'я"),
        lastName: prompt("Введіть прізвище"),
        birthday: prompt("Введіть дату народження в форматі dd.mm.yyyy"),
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function() {
            let now = new Date();
            now = +now;
            let birth = this.birthday.slice(6)+"-"+this.birthday.slice(3,5)+"-"+this.birthday.slice(0,2);
            birth = new Date(birth);
            birth = +birth;
            return parseInt((now-birth)/1000/60/60/24/365);
        },
        getPassword:function() {
            let birth = this.birthday.slice(6)+"-"+this.birthday.slice(3,5)+"-"+this.birthday.slice(0,2);
            birth = new Date(birth);
            return this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+birth.getFullYear();
        }


    }

    console.log(newUser.getLogin());
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
    return newUser;
}
console.log(createNewUser());


